# BSI Installer
A minimal & fast installer for [BetterSearch](https://github.com/RatinA0/BetterSearch) plugin.

## About
Just did a really simple installer for that plugin, in future, maybe there will be a generic version of this installer.

Support offline install if present next to the installer.

## Installation
Run BSI from [releases](https://gitlab.com/princessmortix/bsi-installer/-/releases) page.

## Compiling / Building
1. Clone this repo
2. Install [Go](https://go.dev)
3. On the repository folder, run `go build` to build the application.

## Support
Open an issue, or contact me in [Lyric Community](https://discord.io/edmlyrics).

## Roadmap
[ ] Make a generic installer.

## Contributing
Feel free to open issue (reporting bugs and so on) or make a pull request (to fix broken code).

## Authors and acknowledgment
- Github Copilot;
- [Martin](https://github.com/RatinA0), who created the plugin;
- [Me](https://princessmortix.link).

## License
MIT

## Project status
✅ **Active.**


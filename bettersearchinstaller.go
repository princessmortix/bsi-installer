//Minimal Vegas plugin installer.
package main

import (
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"syscall"
	"time"

	"github.com/ncruces/zenity"
)

type error interface {
	Error() string
}

func main() {
	//Create a progress bar dialog
	dlg, err := zenity.Progress(
		zenity.Title("Installing BetterSearch"),
		zenity.Pulsate(),
	)
	if err != nil {
		return
	}
	defer dlg.Close()
	dlg.Text("Installing, please wait...")
	time.Sleep(time.Second * 1)

	//Get user documents path
	docs, err := os.UserHomeDir()
	if err != nil {
		log.Fatalln(err)
	}

	//Create a new folder if not exists in user documents
	dlg.Text("Creating files")
	if _, err := os.Stat(docs + "/Documents/Vegas Application Plugins"); os.IsNotExist(err) {
		os.Mkdir(docs+"/Documents/Vegas Application Plugins", 0777)
	}

	//Check if user is using Vegas 13 and lower, or 14 and up using a list dialog.
	vversion, err := zenity.List("Which version of Vegas are you using?", []string{"Vegas 13 or lower", "Vegas 14 or higher"}, zenity.Title("What is your Vegas Version"), zenity.DisallowEmpty())
	if vversion == "Vegas 13 or lower" {
		//Download the plugin for Vegas 13 and lower
		err = progressDownloader("https://github.com/RatinA0/BetterSearch/releases/latest/download/BetterSearch13.dll", "BetterSearch.dll", docs+"/Documents/Vegas Application Plugins")
		if err != nil {
			log.Println("Failed to fetch from Github, using local file instead.")
			//Move the local file to the plugin folder
			dlg.Text("Registering plugin")
			err = os.Rename("BetterSearch13.dll", docs+"/Documents/Vegas Application Plugins/BetterSearch.dll")
			if err != nil {
				zenity.Error("Failed to move local file to plugin folder. "+err.Error(), zenity.Title("Error while installing the plugin."), zenity.ErrorIcon)
				os.Exit(1)
			}
		}
	} else if vversion == "Vegas 14 or higher" {
		//Download the plugin for Vegas 14 and lower
		err = progressDownloader("https://github.com/RatinA0/BetterSearch/releases/latest/download/BetterSearch14.dll", "BetterSearch.dll", docs+"/Documents/Vegas Application Plugins")
		if err != nil {
			log.Println("Failed to fetch from Github, using local file instead.")
			//Move the local file to the plugin folder
			dlg.Text("Registering plugin")
			err = os.Rename("BetterSearch14.dll", docs+"/Documents/Vegas Application Plugins/BetterSearch.dll")
			if err != nil {
				zenity.Error("Failed to move local file to plugin folder. "+err.Error(), zenity.Title("Error while installing the plugin."), zenity.ErrorIcon)
				os.Exit(1)
			}
		}
	} else if err != nil {
		log.Fatalln("User cancelled the installation.")
	}

	//Check if DotNet 4.0 is installed
	dlg.Text("Checking if .NET 4.0 is installed")
	time.Sleep(time.Second * 1)
	if _, err := os.Stat("C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\MSBuild.exe"); os.IsNotExist(err) {
		dn4 := zenity.Question("DotNet 4.0 not found. Install it now?", zenity.Title("Question"), zenity.QuestionIcon)
		switch {
		case dn4 != zenity.ErrCanceled:
			//Install DotNet 4.0
			err = progressDownloader("https://dotnet.microsoft.com/download/dotnet-framework/runtime/4.0.1/windows-x64/dotnet-framework-4.0.1-win-x64.exe", "dotnet-framework-4.0.1-win-x64.exe", "")
			//Silently install the DotNet 4.0
			cmd := exec.Command("dotnet-framework-4.0.1-win-x64.exe", "/q", "/norestart")
			cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
			err = cmd.Run()

			if err != nil {
				zenity.Error("Failed to download DotNet 4.0. " + err.Error())
				os.Exit(1)
			}
		case dn4 == zenity.ErrCanceled:
			zenity.Warning("You'll need to install DotNet 4.0 later, else this plugin won't work.", zenity.Title("Warning"), zenity.WarningIcon)
		}
	}
	dlg.Text("Instalaion complete.")
	time.Sleep(time.Second * 1)
	dlg.Close()
	zenity.Info("Installation complete. Enjoy BetterSearch!", zenity.Title("Installation complete."), zenity.InfoIcon)
}

func progressDownloader(link string, name string, path string) error {
	dlg, err := zenity.Progress(
		zenity.Title("Downloading "+name),
		zenity.Pulsate(),
	)
	if err != nil {
		return err
	}
	defer dlg.Close()

	dlg.Text("This may take a while...")

	err = DownloadFile(path+name, link)
	if err != nil {
		return zenity.Error("An error has occurred while downloading the file: "+err.Error(),
			zenity.Title("Error while downloading"),
			zenity.ErrorIcon)
	}

	dlg.Complete()
	dlg.Text("Finished downloading")

	dlg.Close()
	return err
}

func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}
